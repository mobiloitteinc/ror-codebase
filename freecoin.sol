pragma solidity ^0.4.0;

contract FreedomCoin {

  address public owner = 0x3491542173Afe5cAc3e69934B17BDC26AD5073ee;
  string public constant symbol = "FC";
  string public constant name = "Freedom Coin";
  uint8 public constant decimals = 18;
  uint256 public totalSupply = 0;
  uint256 public maxSupply = 100000000;
  uint256 public rate = 10 ** 15 wei;

  uint256 private constant JUNE_01_2018 = 1522822146;
  // uint256 private constant JUNE_01_2018 = 1527791400;
  uint256 private constant JUNE_30_2018 = 1530297000;

  uint256 private constant JUL_01_2018 = 1522822146;
   // uint256 private constant JUL_01_2018 = 1530383400;
  uint256 private constant JUL_31_2018 = 1532975400;

  uint256 private constant minPurchase = 0.001 ether;
  uint256 private constant PRE_SALE_MIN = 10 ** 15 wei;
  uint256 private constant MAIN_SALE_MIN = 10 ** 15 wei;

  uint256 private constant HARD_CAP = 100000000 ;

  mapping(address => uint256) balances;
  mapping(address => mapping (address => uint256)) allowed;

  modifier onlyOwner {
    require(msg.sender == owner);
      _;
  }

event Transfer(address indexed _from, address indexed _to, uint256 _value);
event Approval(address indexed _owner, address indexed _spender, uint256 _value);

function FreedomCoin() public{
  owner = msg.sender;
}

 function inPreSalePeriod() public constant returns (bool) {
        if (now >= JUNE_01_2018 && now < JUNE_30_2018) {
            return true;
        } else {
            return false;
        }
    }

 function inMainSalePeriod() public constant returns (bool) {
        if (now >= JUL_01_2018 && now < JUL_31_2018) {
            return true;
        } else {
            return false;
        }
    }

function () public payable {
  create(msg.sender);
}

function create(address beneficiary)public payable{
    require(beneficiary != address(0));
    require(inPreSalePeriod());

    uint256 amount = msg.value;

    require(amount/rate <= maxSupply);

    if(amount > 0){
      balances[beneficiary] += amount/rate;
      totalSupply += amount/rate;
      maxSupply -= amount/rate;
    }
  }

function balanceOf(address _owner) public constant returns (uint256 balance) {
    return balances[_owner];
}


function balanceMaxSupply() public constant returns (uint256 balance) {
    return maxSupply;
}


function balanceEth(address _owner) public constant returns (uint256 balance) {
    return _owner.balance;
}

function collect(uint256 amount) onlyOwner public{
  msg.sender.transfer(amount);
}

function transfer(address _to, uint256 _amount) public returns (bool success) {
    require(inMainSalePeriod());
    require(_to != address(0));
    if (maxSupply >= _amount && _amount > 0) {
        maxSupply -= _amount;
        balances[_to] += _amount;
        emit Transfer(msg.sender, _to, _amount);
        return true;
    } else {
        return false;
    }
}

function transferFrom(address _from,address _to,uint256 _amount) public returns (bool success) {
    
    require(inMainSalePeriod());
    require(_from != address(0));
    require(_to != address(0));
    require(balances[_from] >= _amount && allowed[_from][msg.sender] >= _amount && balances[_to] + _amount >= balances[_to]);

    if (balances[_from] >= _amount
        && allowed[_from][msg.sender] >= _amount
        && _amount > 0
        && balances[_to] + _amount > balances[_to]) {
        balances[_from] -= _amount;
        allowed[_from][msg.sender] -= _amount;
        balances[_to] += _amount;
        emit Transfer(_from, _to, _amount);
        return true;
    } else {
        return false;
    }
}

function approve(address _spender, uint256 _amount) public returns (bool success) {
    allowed[msg.sender][_spender] = _amount;
    emit Approval(msg.sender, _spender, _amount);
    return true;
}

function allowance(address _owner, address _spender) public constant returns (uint256 remaining) {
    return allowed[_owner][_spender];
}

}